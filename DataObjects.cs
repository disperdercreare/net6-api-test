﻿using System.Text.Json.Serialization;

namespace web_test;

public record Status(
    string Info,
    string Name,
    [property: JsonConverter(typeof(DateOnlyConverter))]
    DateOnly CreatedAt,
    [property: JsonConverter(typeof(DateOnlyConverter))]
    DateOnly ModifiedAt);