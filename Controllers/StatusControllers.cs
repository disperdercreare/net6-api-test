﻿namespace web_test.Controllers;

public class StatusControllers
{
    public static void MapControllers(WebApplication app)
    {
        app.MapGet("api/v1/applicants/client/{clientId}/orderStatus",
            async (string clientId, IService service) =>
            {
                var status = await service.GetStatus(clientId);
                return Results.Ok(status);
            });
    }
}

public interface IService
{
    Task<Status> GetStatus(string clientId);
}

public class Service : IService
{
    public Task<Status> GetStatus(string clientId)
    {
        return Task.Run(()=>
            new Status("one", "john", DateOnly.MinValue, DateOnly.MinValue
            ));
    }
}